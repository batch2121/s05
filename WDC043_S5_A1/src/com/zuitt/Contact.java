package com.zuitt;

import java.util.ArrayList;

public class Contact {

    private String name;
    private ArrayList<String> numbers;
    private ArrayList<String> addresses;

    Contact(){};

    Contact(String name, ArrayList<String> numbers, ArrayList<String> addresses){
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;
    }

    public String getName(){
        return this.name;
    }

    public ArrayList<String> getNumbers(){
        return this.numbers;
    }

    public ArrayList<String> getAddresses(){
        return this.addresses;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setNumbers(ArrayList<String> numbers){
        this.numbers = numbers;
    }

    public void setAddresses(ArrayList<String> addresses){
        this.addresses = addresses;
    }


}
