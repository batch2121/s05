package com.zuitt;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;

public class Phonebook extends Contact{

    public ArrayList<String> contacts;
    Phonebook(){}

    public void setContacts(String name, ArrayList<String> numbers, ArrayList<String> addresses){
        this.setName(name);
        this.setNumbers(numbers);
        this.setAddresses(addresses);
    }

    public void getContacts(){
        System.out.println(getName());
        System.out.println("----------------------------------------------------------------------------------");
        System.out.println(getName() + " has the following registered numbers:");
        for (String number : getNumbers()) {
            System.out.println(number);
        }
        System.out.println("----------------------------------------------------------------------------------");
        System.out.println(getName() + " has the following registered addresses:");
        for (int i = 0; i < getAddresses().size(); i++){
            System.out.println(getAddresses().get(i));
        }
        System.out.println("===================================================================================");
    }


}
