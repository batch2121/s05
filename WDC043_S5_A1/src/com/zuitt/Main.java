package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Phonebook contact = new Phonebook();
        Phonebook contact1 = new Phonebook();
        contact.setContacts("Johnro Lebaste", new ArrayList<>(Arrays.asList("09222222222", "09555555555")), new ArrayList<>(Arrays.asList("Manila", "Bulacan")));
        contact1.setContacts("Nate River", new ArrayList<>(Arrays.asList("09123123123", "09321321321")), new ArrayList<>(Arrays.asList("Batanes", "Jolo")));
        contact.getContacts();
        contact1.getContacts();

    }
}
